all:
	cmake -B build -S .
	cmake --build build --parallel --verbose
	build/cmake-external-project

entr:
	ls CMakeLists.txt Makefile main.cxx | entr -cr make

clean:
	$(RM) -r build
